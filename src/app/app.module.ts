import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//import { ChartsModule, WavesModule } from 'angular-bootstrap-md';
//import { MDBSpinningPreloader } from 'ng-uikit-pro-standard';
//import { NgApexchartsModule } from "ng-apexcharts"

//import page modules
import { DashboardModule } from './pages/dashboard/dashboard.module';
import {ProgressBarModule} from './components/progress-bar/progress-bar-chart.module';
import {RadialBarModule} from './components/radial-bar/radial-bar-chart.module';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule,
    ProgressBarModule,
    RadialBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
