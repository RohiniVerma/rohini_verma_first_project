import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';
//import { MDBSpinningPreloader } from 'ng-uikit-pro-standard';
import { NgApexchartsModule } from "ng-apexcharts";
import {ProgressBarModule} from '../../components/progress-bar/progress-bar-chart.module';
import { RadialBarModule } from '../../components/radial-bar/radial-bar-chart.module';
import {ColumnBarModule} from '../../components/column-bar/column-bar-chart.module';
import {NgbPopoverModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports: [
        BrowserModule,
        DashboardRoutingModule,
        ChartsModule,
        WavesModule,
        NgApexchartsModule,
        ProgressBarModule,
        RadialBarModule,
      ColumnBarModule,
      NgbPopoverModule


    ],
    exports:[DashboardComponent]
})
export class DashboardModule { }
