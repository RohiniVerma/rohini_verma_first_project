import { Component,   } from '@angular/core';
@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

myArrayTypes :any = {
   data:[ {id: 1, value: '50',label:'Annual'},
    {id: 2, value: '75',label:'H1'},
    {id: 3, value: '100',label:'H2'}
    ],
    graphType:0,
    name:'RBI Cyber Security',
    height:200,
    color:'#85BC20'
      
};
myArrayTypes1 :any = {
  data:[ {id: 1, value: '40',label:'Annual', height:200},
   {id: 2, value: '63',label:'H1'},
   {id: 3, value: '97',label:'H2'}
   ],
   graphType:0,
   name:'IDPR',
   height:200,
   color:"#FF6347"
     
};
myArrayTypes2 :any = {
  data:[ {id: 1, value: '15',label:'Annual'},
   {id: 2, value: '45',label:'H1'},
   {id: 3, value: '83',label:'H2'}
   ],
   graphType:0,
   name:'Control Library',
   height:200,
   color:'#6A5ACD'

     
};
radialProgressBar:any=
{
  data:[ {id: 1, value: '67',label:'67',  height:130},],
  graphType:0,
  name:'PCI DSS',
  height:130,
  color:'#85BC20'

}
radialProgressBar1:any=
{
  data:[ {id: 1, value: '46',label:'46'},],
  graphType:0,
  name:'ISMS',
  height:130,
  color:'#6A5ACD',
 }
radialProgressBar2:any=
{
  data:[ {id: 1, value: '15',label:'15',height:130},],
  graphType:0,
  name:'CSA',
  height:130,
  color:"#FF6347"
  }
radialProgressBar3:any=
{
  data:[ {id: 1, value: '67',label:'67',height:130},],
  graphType:0,
  name:'GDPR',
  height:130,
  color:'#1E90FF'
}
  

 constructor() {
    }

    ngOnInit() {
 }
   progressChart(myArrayTypes)
  {
    myArrayTypes.graphType=0;
   }
  radialChart(myArrayTypes)
  {
    myArrayTypes.graphType=1;
  }
  columnChart(myArrayTypes)
  {
    myArrayTypes.graphType=2;
  }
  togglePopOver(popover, myArrayTypes) {
    if (popover.isOpen()) {
      popover.close();
    } else {
      popover.open({myArrayTypes});
    }
  }

     
    

}