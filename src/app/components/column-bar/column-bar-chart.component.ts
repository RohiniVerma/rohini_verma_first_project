import { Component,ViewChild, Input} from '@angular/core';

import {
    ApexPlotOptions,
      ApexChart
  } from "ng-apexcharts";
  
  import {
     ApexDataLabels,
      ApexYAxis,
      ApexAnnotations,
      ApexFill,
      ApexStroke,
      ApexGrid
  } from "ng-apexcharts";
  
  export type ChartOptions = {
    // series: ApexNonAxisChartSeries;
    chart: ApexChart;
    labels: string[];
    // plotOptions: ApexPlotOptions;

    series: any;
    // chart: ApexChart;
    dataLabels: ApexDataLabels;
    plotOptions: ApexPlotOptions;
    yaxis: ApexYAxis;
    xaxis: any; //ApexXAxis;
    annotations: ApexAnnotations;
    fill: ApexFill;
    stroke: ApexStroke;
    grid: ApexGrid;
};
 
@Component({
  selector: 'app-column-bar-chart',
  templateUrl: './column-bar-chart.component.html',
styleUrls: ['./column-bar-chart.component.css']
})
export class ColumnBarComponent {
    @ViewChild("chartColumnType", null) chartColumnType: any;
    public ChartOptionsColumnType: Partial<ChartOptions>;
  @Input()myArrayTypes:any
constructor(){
}

ngOnInit() {
    let series =this.myArrayTypes.data.map((item)=>{ 
        return item.value;
      });
      let labels =this.myArrayTypes.data.map((item)=>{ 
        return item.label;
          });
    this.ChartOptionsColumnType = {
        series: [
            {
                name: "Servings",
                data: series
            }
        ],
        // annotations: {
        //     points: [
        //         {
        //             x: "Bananas",
        //             seriesIndex: 0,
        //             label: {
        //                 borderColor: "#775DD0",
        //                 offsetY: 0,
        //                 style: {
        //                     color: "#fff",
        //                     background: "#775DD0"
        //                 },
        //                 text: "Bananas are good"
        //             }
        //         }
        //     ]
        // },
        chart: {
            height: 200,
            type: "bar"
        },
        plotOptions: {
            bar: {
                columnWidth: "20%",
                endingShape: "rounded"
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: 2
        },

        grid: {
            row: {
                colors: ["#fff", "#f2f2f2"]
            }
        },
        xaxis: {
            labels: {
                rotate: -45
            },
            // categories: [
            //   "Apples",
            //   "Oranges",
            //   "Strawberries",
            //   "Pineapples",
            //   "Mangoes",
            //   "Bananas",
            //   "Blackberries",
            //   "Pears",
            //   "Watermelons",
            //   "Cherries",
            //   "Pomegranates",
            //   "Tangerines",
            //   "Papayas"
            // ],
            tickPlacement: "on"
        },
        yaxis: {
            title: {
                text: "Servings"
            }
        },
        fill: {
            type: "gradient",
            gradient: {
                shade: "light",
                type: "horizontal",
                shadeIntensity: 0.25,
                gradientToColors: undefined,
                inverseColors: true,
                opacityFrom: 0.85,
                opacityTo: 0.85,
                stops: [50, 0, 100]
            }
        }
    };

   
}

}