import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ColumnBarComponent } from './column-bar-chart.component';

const routes: Routes = [
    { path:'columnbar', component:ColumnBarComponent },
    { path:'', component:ColumnBarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ColumnBarRoutingModule { }