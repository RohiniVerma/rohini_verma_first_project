import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';

import { NgApexchartsModule } from "ng-apexcharts";
import {ColumnBarComponent} from './column-bar-chart.component';
import{ColumnBarRoutingModule} from './column-bar-chart-routing.module';


@NgModule({
    declarations: [
        ColumnBarComponent
    ],
    imports: [
        BrowserModule,
        ColumnBarRoutingModule,
        ChartsModule,
        WavesModule,
        NgApexchartsModule
    ],
    exports:[ColumnBarComponent],
   
})
export class ColumnBarModule { }
