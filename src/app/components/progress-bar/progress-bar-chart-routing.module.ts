import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgressBarComponent } from './progress-bar-chart.component';

const routes: Routes = [
    { path:'progressbar', component:ProgressBarComponent },
    { path:'', component:ProgressBarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ProgressBarRoutingModule { }