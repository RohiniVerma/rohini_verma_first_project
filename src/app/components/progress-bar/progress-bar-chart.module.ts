import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';

import { NgApexchartsModule } from "ng-apexcharts";
import {ProgressBarComponent} from './progress-bar-chart.component';
import{ProgressBarRoutingModule} from './progress-bar-chart-routing.module';


@NgModule({
    declarations: [
        ProgressBarComponent
    ],
    imports: [
        BrowserModule,
        ProgressBarRoutingModule,
        ChartsModule,
        WavesModule,
        NgApexchartsModule,
      
    ],
    exports:[ProgressBarComponent],
   
})
export class ProgressBarModule { }
