import { Component,ViewChild, Input} from '@angular/core';

import {
    ApexNonAxisChartSeries,
    ApexPlotOptions,
    ApexChart
} from "ng-apexcharts";

import {
    ApexAxisChartSeries,
    ChartComponent,
    ApexDataLabels,
    ApexYAxis,
    ApexAnnotations,
    ApexFill,
    ApexStroke,
    ApexGrid
} from "ng-apexcharts";
export type ChartOptions = {
    // series: ApexNonAxisChartSeries;
    chart: ApexChart;
    labels: string[];
    // plotOptions: ApexPlotOptions;

    series: any;
    // chart: ApexChart;
    dataLabels: ApexDataLabels;
    plotOptions: ApexPlotOptions;
    yaxis: ApexYAxis;
    xaxis: any; //ApexXAxis;
    annotations: ApexAnnotations;
    fill: ApexFill;
    stroke: ApexStroke;
    grid: ApexGrid;
    colors: string[];
};
 
@Component({
  selector: 'app-radial-bar-chart',
  templateUrl: './radial-bar-chart.component.html',
styleUrls: ['./radial-bar-chart.component.css']
})


export class RadialBarComponent {

 @ViewChild("chart", null) chart: any;
    public chartOptions: Partial<ChartOptions>;
 
    @Input() myArrayTypes:any;
constructor(){
}

ngOnInit() {
  debugger
 
  let series =this.myArrayTypes.data.map((item)=>{ 
    return item.value;
  });
  let labels =this.myArrayTypes.data.map((item)=>{ 
    return item.label;
      });
      // let height=this.myArrayTypes.data.map((item)=>{
      //   return item.height;
      // })
    this.chartOptions = {
        series: series,
        chart: {
          height: this.myArrayTypes.height,
          type: "radialBar"
        },
        plotOptions: {
          radialBar: {
            dataLabels: {
              name: {
                fontSize: "22px",
              },
              value: {
                fontSize: "16px",
              },
              
          
            }
          }
        },
         colors: [this.myArrayTypes.color],
        labels:labels
      };
    
    // else{
    //   this.chartOptions = {
      
    //     series: [70],
    //     chart: {
    //       height: 150,
    //       type: "radialBar"
    //     },
    //     plotOptions: {
    //       radialBar: {
    //         dataLabels: {
    //           name: {
    //             fontSize: "22px"
    //           },
    //           value: {
    //             fontSize: "16px"
    //           },
          
    //         }
    //       }
    //     },
    //     labels: ["70%"]
    //   };
    // }
 
 
  }
 };

   

  





