import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RadialBarComponent } from './radial-bar-chart.component';

const routes: Routes = [
    { path:'radialbar', component:RadialBarComponent },
    { path:'', component:RadialBarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RadialBarRoutingModule { }