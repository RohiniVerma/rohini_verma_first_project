import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';

import { NgApexchartsModule } from "ng-apexcharts";
import {RadialBarComponent} from './radial-bar-chart.component';
import{RadialBarRoutingModule} from './radial-bar-chart-routing.module';


@NgModule({
    declarations: [
        RadialBarComponent
    ],
    imports: [
        BrowserModule,
        RadialBarRoutingModule,
        ChartsModule,
        WavesModule,
        NgApexchartsModule
    ],
    exports:[RadialBarComponent],
   
})
export class RadialBarModule { }
